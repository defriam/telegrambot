// packages
const Telegraf = require('telegraf');
const urlRegex = require('url-regex');
const fetch = require('node-fetch');
const Markup = require('telegraf/markup');
const ent = require('ent');
const db = require('./db');

// init bot
const bot = new Telegraf(process.env.BOT_TOKEN);

// regex
const titleRegex = /<title>(.+?)<\/title>/gim;

// memory storage
const memoryStore = {};

const categories = ['Article', 'Release', 'Lib', 'Blogs', 'Elearning'];
// basic message
const welcomeMessage = `Welcome!
I am Defria.io Telegram Bot.
Sende me your links to store them.`;

const helpMessage = `Send me a link`;
// basic commands
bot.start((ctx) => ctx.reply(welcomeMessage));
bot.help((ctx) => ctx.reply(helpMessage));

// listen to commands
// bot.on('sticker', (ctx) => ctx.reply('👍'));
bot.hears(/New episode (.+)/, async (ctx) => {
    const userId = ctx.from.id;
    const episodeName = ctx.match[1];

    // remove all old episode
    await db.remove({ userId: userId }, { multi: true });

    // create new
    await db.insert({ userId, episodeName, links: [] });

    ctx.reply(`New episode created with name: ${episodeName}`);
});
bot.hears(urlRegex(), async (ctx) => {
    // get urls from message text
    const urls = ctx.message.text.match(urlRegex());
    const firsUrl = urls[0];
    // get url title
    const body = await fetch(firsUrl).then(r => r.text());
    const titleTag = body.match(titleRegex);
    try {
        const title = ent.decode(titleTag[0].replace('<title>', '').replace('</title>', ''));
        // get userId
        const userId = ctx.from.id;
        memoryStore[userId] = {
            url: firsUrl,
            title,
            category: ctx.message.text
        };

        const currentCollection = await db.find({
            userId
        });

        ctx.reply(`Ready to save: ${title}
    What category should it be?`, Markup.keyboard(categories).oneTime().resize().extra());
    } catch (error) {

    }
});

categories.forEach(category => {
    bot.hears(category, async (ctx) => {
        const userId = ctx.from.id;
        const linkObject = memoryStore[userId]; //= {url:firsUrl, title, category: ''}; 
        linkObject.category = category;
        await db.update({ userId }, { $push: { links: linkObject } });
        ctx.reply(`Saving link into ${category} : ${memoryStore[userId].title}`);
    });
});

// start bot
bot.startPolling();